#!/bin/bash
shopt -s autocd # cd into directory by just typing the directory name
HISTSIZE=1000.

# colorful manpages
man() {
    LESS_TERMCAP_mb=$(tput setaf 4)\
    LESS_TERMCAP_md=$(tput setaf 4;tput bold) \
    LESS_TERMCAP_so=$(tput setaf 7;tput setab 4;tput bold) \
    LESS_TERMCAP_us=$(tput setaf 6) \
    LESS_TERMCAP_me=$(tput sgr0) \
    LESS_TERMCAP_se=$(tput sgr0) \
    LESS_TERMCAP_ue=$(tput sgr0) \
    command man "$@"
}

# get weather for city
weather() {
    city="$1"

    if [ -z "$city" ]; then
        city="New_York"
    fi

    eval "curl http://wttr.in/${city}"
}

# go to directory previously copied to clipboard
function godr() {
    clip=$(xclip -o)
    if [ -d "$clip" ]; then
        cd $clip
    else
        echo 'No such dir...'    
	fi
}

# rm command is interactive. This command pipe the yes command to rm.
function rmdir() {
    while true; do
        read -p "Directory will be removed permanently. Proceed? " resp
        case $resp in
            [Yy]* ) yes | rm -r ${1}; echo "Dir '${1}' removed." ; break;;
            [Nn]* ) echo "Aborted."; break;;
            * ) echo "Please answer yes or no.";;
        esac
    done
}

function p6file() {
    shebang='#!/usr/bin/env perl6'
    echo -e "${shebang}\n\n" > "${1}.p6"
}

function p6() {
    if [ "$1" == "-h" ]; then
        echo "Usage: lib6 script.p6 [module-path=.]"
        return 1
    fi

    # launch the REPL

    # create a .p6 file with shebang

    # execute .p6 script with modified include path
    if [ -z "$2" ]; then
        perl6 $1
    else
        PERL6LIB=${2} perl6 $1
    fi
}

#-------------------------
# PROMPTING--GENERAL SETUP
#-------------------------

# --- START: prompt ---
function prompt() {
    NORMAL="\[\033[00m\]"
    WHITE="\[\033[01;38;5;018m\]"
    GREEN="\[\033[01;38;5;004m\]"
    BLUE="\[\033[01;38;5;008m\]"
    PINK="\[\033[01;38;5;162m\]"
    RED="\[\033[01;38;5;003m\]"

    my_ps_host="${GREEN}\h${NORMAL}";
    my_ps_user="${BLUE}\u${NORMAL}";
    my_ps_dir='$(basename $(dirname "$PWD"))/$(basename "$PWD")';
    
    if [ -n "$VIRTUAL_ENV" ]; then
        ve=`basename $VIRTUAL_ENV`;
    fi

    PS1="┌─[$(date +%R)][${PINK}$my_ps_dir${NORMAL}]
└─⚬ ";

    PS1="\[$(tput bold)\]\[$(tput setaf 1)\][\[$(tput setaf 3)\]\u\[$(tput setaf 2)\]@\[$(tput setaf 4)\]\h \[$(tput setaf 5)\]\W\[$(tput setaf 1)\]]\[$(tput setaf 7)\]\\$ \[$(tput sgr0)\]"

    PS1="[${PINK}$my_ps_dir${NORMAL}] \n$ ";
}

PROMPT_COMMAND=prompt
# --- END: prompt ---


[ -f "$HOME/.config/shortcutrc" ] && source "$HOME/.config/shortcutrc"
[ -f "$HOME/.config/aliasrc" ] && source "$HOME/.config/aliasrc"
