
#!/bin/sh
# Profile file. Runs on login.

export EDITOR="vim"
export TERMINAL="xfce4-terminal"
export BROWSER="firefox"
export READER="zathura"
export NOTMUCH_CONFIG="$HOME/.config/notmuch-config"
export GTK2_RC_FILES="$HOME/.gtkrc-2.0"
export QT_QPA_PLATFORMTHEME="qt5ct"
export LATEXTEMPLATES="$HOME/.dotfiles/templates/eisvogel.tex"

PATH="/home/luis/perl5/bin${PATH:+:${PATH}}"; 
PATH="/opt/rakudo/install/bin/${PATH:+:${PATH}}"
PATH="/home/luis/.perl6/bin/${PATH:+:${PATH}}"
PATH="/opt/rakudo/modules/${PATH:+:${PATH}}"

# Adds `~/.scripts` and all subdirectories to $PATH
PATH="$PATH:$(du "$HOME/.scripts/" | cut -f2 | tr '\n' ':' | sed 's/:*$//')"

export PATH

[ ! -f ~/.config/shortcutrc ] && shortcuts >/dev/null 2>&1

echo "$0" | grep "bash$" >/dev/null && [ -f ~/.bashrc ] && source "$HOME/.bashrc"
