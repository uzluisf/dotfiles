#!/bin/sh

PKGLIST=(
qmpdclient
git
vim
zathura
pdfshuffler
gedit
keepassxc
hexchat
firefox
viewnior
gpick
rofi
compton
i3-gaps
nitrogen
lxappearance
redshift
keepassxc
adapta-gtk-theme
inkscape
qutebrowser
nitrogen
)

pacman -Sy --color always

for pkg in "${PKGLIST[@]}"; do
    if pacman -Qi "$pkg" > /dev/null 2>&1; then
        echo -e "» $pkg: Already installed."
    else
        pacman -S --noconfirm "$pkg" > /dev/null 2>&1 &&
	    echo "✓ $pkg: Successfuly installed!"
    fi
done

