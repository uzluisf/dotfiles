"-------------
" PLUGINS SETUP
"-------------

if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

call plug#begin('~/.vim/plugged')
Plug 'https://github.com/Shougo/neocomplete.vim.git'
Plug 'https://github.com/vim-perl/vim-perl6.git'
call plug#end()

" Neocomplete
let g:neocomplete#enable_at_startup = 1

"-------------
" VIM BEHAVIOR
"-------------

set nocompatible
set encoding=utf-8
" disable unsafe commands in local .vimrc files
set secure
" no backup or swap
set nobackup nowritebackup noswapfile autoread
" highlight matches, search as entered, highlight {([])}
set hlsearch incsearch showmatch
set ignorecase smartcase
" autocomplete when using the command menu
set wildmenu
" sane backspace
set backspace=indent,eol,start
" use the system clipboard for yank/put/delete
set clipboard+=unnamed,unnamedplus
" enable mouse for all modes settings
set mouse=a
" hide the mouse cursor while typing
set mousehide
" right-click pops up context menu
set mousemodel=popup
" show cursor position in status bar
set ruler
" show absolute line number of the current line
set number
" disable code folding 
set nofoldenable
" scroll the window so we can always see 10 lines around the cursor
set scrolloff=10
" wrap at 80 characters
set textwidth=80
" always show status bar
set laststatus=2
" show line numbers
set number
" show last command entered in bottom right
set showcmd
" set default shell
set shell=bash
" enable syntax processing 
syntax on
" auto save
autocmd BufLeave,CursorHold,CursorHoldI,FocusLost * silent! wa
" Load filetype-specific(for example, .pl) indent files                
filetype plugin on
filetype indent on
" Keep working dir clean
set directory=~/.tmp//,/tmp//,.
set backupdir=~/.tmp//,/tmp//,.

"-----------
" APPEARANCE
"-----------

set background=dark
colorscheme solarized
" highligh background from column 100
let &colorcolumn="80,".join(range(80,999),",")
" Underline current cursor position
set cursorline
" number of visual spaces per TAB
set tabstop=4
" indents will have a width of 4
set shiftwidth=4
" number of spaces a TAB counts when editing
set softtabstop=4
" expand tabs into spaces
set expandtab
" enable indentation
set autoindent
" reduce delay between mode change
set ttimeoutlen=0
" blinking block when in normal/virtual mode       
let &t_SI = "\e[6 q"
" steady I-beam when in insert mode
let &t_EI = "\e[1 q"

"-----------
" SHORTCUTS
"-----------

" set mapleader key
let mapleader = '\'
" Create new split and switch to it.
nnoremap <Leader>hs <C-w>v<C-w>l
nnoremap <Leader>vs <C-w>s<C-w>j
" Close split
nnoremap <Leader>c <C-w>c

" Easier movement in split windows.
nnoremap <Leader>h <C-w>h
nnoremap <Leader>j <C-w>j
nnoremap <Leader>k <C-w>k
nnoremap <Leader>l <C-w>l

"---------
" MAPPINGS
"---------

" disable arrow keys in insertion mode
inoremap <Up> <Nop>
inoremap <Down> <Nop>
inoremap <Right> <Nop>
inoremap <Left> <Nop>
" disable arrow keys in virtual mode
vnoremap <Up> <Nop>
vnoremap <Down> <Nop>
vnoremap <Right> <Nop>
vnoremap <Left> <Nop>
" disable arrow keys in normal mode
noremap <Up> <Nop>
noremap <Down> <Nop>
noremap <Right> <Nop>
noremap <Left> <Nop>
" tap Esc twice to clear search highlights
nnoremap <silent> <Esc><Esc> <Esc>:nohlsearch<CR><Esc>
" tap jj twice to exit insert mode
inoremap jj <esc>
" reload ~/.vimrc
noremap \rs :source ~/.vimrc<CR>
" Pressing Control-j to jump to placeholder
inoremap <c-j> <Esc>/<++><CR><Esc>cf>

"------------------
" FYLETYPE SPECIFIC
"------------------
filetype plugin on

" RAKU PERL 6
au FileType perl6 setlocal textwidth=80

" multi-line comment in insert mode
autocmd Filetype perl6 inoremap ;mc #\|«<Enter><Enter>»<Esc>kA

" Pod formatting
autocmd Filetype pod,pod6,perl6 inoremap ;b B<><++><Esc>F<hi
autocmd Filetype pod,pod6,perl6 inoremap ;c C<><++><Esc>F<hi
autocmd Filetype pod,pod6,perl6 inoremap ;i I<><++><Esc>F<hi
autocmd Filetype pod,pod6,perl6 inoremap ;u U<><++><Esc>F<hi
autocmd Filetype pod,pod6,perl6 inoremap ;a L<\|<++>><++><Esc>F\|i
autocmd Filetype pod,pod6,perl6 inoremap ;n N<><++><Esc>F<hi
autocmd Filetype pod,pod6,perl6 inoremap ;h1 =head1<Space><Enter><++><Esc>kA
autocmd Filetype pod,pod6,perl6 inoremap ;h2 =head2<Space><Enter><++><Esc>kA
autocmd Filetype pod,pod6,perl6 inoremap ;h3 =head3<Space><Enter><++><Esc>kA
autocmd Filetype pod,pod6,perl6 inoremap ;h4 =head4<Space><Enter><++><Esc>kA
autocmd Filetype pod,pod6,perl6 inoremap ;i1 =head1<Space><Enter><++><Esc>kA
autocmd Filetype pod,pod6,perl6 inoremap ;i2 =head2<Space><Enter><++><Esc>kA
autocmd Filetype pod,pod6,perl6 inoremap ;i3 =head3<Space><Enter><++><Esc>kA
autocmd Filetype pod,pod6,perl6 inoremap ;i4 =head4<Space><Enter><++><Esc>kA
autocmd Filetype pod,pod6,perl6 inoremap ;pod =begin pod<Enter><Enter>=end pod<Esc>kA
autocmd Filetype pod,pod6,perl6 inoremap ;pco =begin comment<Enter><Enter>=end comment<Esc>kA
autocmd Filetype pod,pod6,perl6 inoremap ;pcd =begin code<Enter><Enter>=end code<Esc>kA

" MARKDOWN
autocmd Filetype markdown inoremap ;b ****<++><Esc>F*hi
autocmd Filetype markdown inoremap ;c ``<++><Esc>F`i
autocmd Filetype markdown inoremap ;i **<++><Esc>F*i
autocmd Filetype markdown inoremap ;s ~~~~<++><Esc>F~hi
autocmd Filetype markdown inoremap ;a [](<++>)<++><Esc>F[a
autocmd Filetype markdown inoremap ;p ![](<++>)<++><Esc>F[a
autocmd Filetype markdown inoremap ;h ---<Enter><Enter>
autocmd Filetype markdown inoremap ;h1 #<Space><Enter><++><Esc>kA
autocmd Filetype markdown inoremap ;h2 ##<Space><Enter><++><Esc>kA
autocmd Filetype markdown inoremap ;h3 ###<Space><Enter><++><Esc>kA
autocmd Filetype markdown inoremap ;h4 ####<Space><Enter><++><Esc>kA

" LATEX
" Recognize .tex files as latex (instead of plaintex) for syntax highlighting.
let g:tex_flavor="latex"
" Compile document, be it groff/LaTeX/markdown/etc.
map <leader>c :silent w! \| !compiler <c-r>%<CR><CR>
" Open corresponding .pdf/.html or preview
map <leader>p :silent !opout <c-r>%<CR><CR>

" ALL
" Disable auto-commenting. See :help fo-table for more info
autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o

" Inserting shebang for * file
autocmd FileType * inoremap ;#! #!/usr/bin env <Enter><Enter><++><Esc>2kA

